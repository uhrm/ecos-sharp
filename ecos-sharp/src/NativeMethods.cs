
//  
//  NativeMethods.cs
//  ecos-sharp
//
//  Copyright 2024 Markus Uhr <uhrmar@gmail.com>.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in
//        the documentation and/or other materials provided with the
//        distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.Marshalling;

namespace Ecos;

[CustomMarshaller(typeof(string), MarshalMode.ManagedToUnmanagedOut, typeof(LibraryOwnedUtf8StringMarshaller))]
internal unsafe static class LibraryOwnedUtf8StringMarshaller
{
    public static string? ConvertToManaged(byte* val) => Utf8StringMarshaller.ConvertToManaged(val);
}

internal unsafe struct LpCone
{
    internal long p;         // dimension of cone
    internal double* w;      // scalings
    internal double* v;      // = w^2 - saves p multiplications
    internal long* kkt_idx;  // indices of KKT matrix to which scalings w^2 map
}

internal unsafe struct SoCone
{
    internal long p;             // dimension of cone
    internal double* skbar;      // temporary variables to work with
    internal double* zkbar;      // temporary variables to work with
    internal double a;           // = wbar(1)
    internal double d1;          // first element of D
    internal double w;           // = q'*q
    internal double eta;         // eta = (sres / zres)^(1/4)
    internal double eta_square;  // eta^2 = (sres / zres)^(1/2)
    internal double* q;          // = wbar(2:end)
    internal long* Didx;         // indices for D
    internal double u0;          // eta
    internal double u1;          // u = [u0; u1*q]
    internal double v1;          // v = [0; v1*q]
}

internal unsafe struct Cone
{
    internal LpCone* lpc;      // LP cone
    internal SoCone* soc;      // Second-Order cone
    internal long nsoc;        // number of second-order cones
    void* expc;                // array of exponential cones TODO: expcone
    internal long nexc;        // number of exponential cones
    internal long fexv;        // Index of first slack variable corresponding to an exponential cone
}

internal struct Settings
{
	internal double gamma;         // scaling the final step length
	internal double delta;         // regularization parameter
    internal double eps;           // regularization threshold
	internal double feastol;       // primal/dual infeasibility tolerance
	internal double abstol;        // absolute tolerance on duality gap
	internal double reltol;        // relative tolerance on duality gap
    internal double feastol_inacc; // primal/dual infeasibility relaxed tolerance
	internal double abstol_inacc;  // absolute relaxed tolerance on duality gap
	internal double reltol_inacc;  // relative relaxed tolerance on duality gap
	internal long nitref;           // number of iterative refinement steps
	internal long maxit;            // maximum number of iterations
    internal long verbose;          // verbosity (bool for PRINTLEVEL < 3)
    internal long max_bk_iter;      // Maximum backtracking iterations
    internal double bk_scale;      // Backtracking scaling
    internal double centrality;    // Centrality bound, ignored when centrality vars = 0
}

internal struct Stats
{
	internal double pcost;
	internal double dcost;
    internal double pres;
    internal double dres;
    internal double pinf;
    internal double dinf;
	internal double pinfres;
    internal double dinfres;
    internal double gap;
    internal double relgap;
	internal double sigma;
    internal double mu;
    internal double step;
    internal double step_aff;
	internal double kapovert;
    internal long iter;
    internal long nitref1;
    internal long nitref2;
    internal long nitref3;
    // profiling
    internal double tsetup;
    internal double tsolve;
    /* Counters for backtracking, each of these counts
     * one condition that can fail and cause a backtrack
     */
    internal long pob; /* Potential decreases    */
    internal long cb;  /* Centrality violations  */
    internal long cob; /* The s'z of one cone is too small w.r.t. mu */
    internal long pb;  /* Primal infeasibility   */
    internal long db;  /* Dual infeasibility     */
    internal long affBack; /* Total affine backtracking steps   */
    internal long cmbBack; /* Total combined backtracking steps */

    internal double centrality; /*Centrality at the end of the backtracking*/
}

internal unsafe struct PWork
{
	/* dimensions */
    internal long n;      // number of primal variables x
    internal long m;      // number of conically constrained variables s
    internal long p;      // number of equality constraints
    internal long D;      // degree of the cone

    /* variables */
    internal double* x;  // primal variables
    internal double* y;  // multipliers for equality constaints
    internal double* z;  // multipliers for conic inequalities
    internal double* s;  // slacks for conic inequalities
    internal double* lambda; // scaled variable
    internal double kap; // kappa (homogeneous embedding)
    internal double tau; // tau (homogeneous embedding)

    /* variables */
    double* best_x;  // primal variables
    double* best_y;  // multipliers for equality constaints
    double* best_z;  // multipliers for conic inequalities
    double* best_s;  // slacks for conic inequalities
    double best_kap; // kappa (homogeneous embedding)
    double best_tau; // tau (homogeneous embedding)
    double best_cx;
    double best_by;
    double best_hz;
    Stats* best_info; // info of best iterate

	/* temporary stuff holding search direction etc. */
    double* dsaff;
    double* dzaff;
    double* W_times_dzaff;
    double* dsaff_by_W;
    double* saff;
    double* zaff;

    /* cone */
    internal Cone* C;

    /* problem data */
    void* A; // TODO: spmat struct
    void* G; // TODO: spmat struct
    double* c;
    double* b;
    double* h;

    /* indices that map entries of A and G to the KKT matrix */
    long* AtoK;
    long* GtoK;

    /* equilibration vector */
    double* xequil;
    double* Aequil;
    double* Gequil;

	/* scalings of problem data */
	double resx0;
    double resy0;
    double resz0;

	/* residuals */
	double* rx;
    double* ry;
    double* rz;
    double rt;
	double hresx;
    double hresy;
    double hresz;

    /* norm iterates */
    double nx;
    double ny;
    double nz;
    double ns;

	/* temporary storage */
	double cx;
    double by;
    double hz;
    double sz;

	/* KKT System */
	void* KKT; // TODO: kkt struct

	/* info struct */
    internal Stats* info;

	/* settings struct */
	internal Settings* settings;
}

internal unsafe partial class NativeMethods
{

    [LibraryImport("ecos")]
    internal static partial PWork* ECOS_setup(
        long n,         // Number of variables
        long m,         // Number of inequalities, number of rows of G
        long p,         // Number of equality constraints
        long l,         // Dimension of positive orthant
        long ncones,    // Number of second order cones
        long[]? q,      // Array of length 'ncones', defines the dimension of each cone
        long nex,       // Number of exponential cones
        double[]? Gpr,  // Sparse G matrix data array (column compressed storage)
        long[]? Gjc,    // Sparse G matrix column index array (column compressed storage)
        long[]? Gir,    // Sparse G matrix row index array (column compressed storage)
        double[]? Apr,  // Sparse A matrix data array (column compressed storage) (can be all NULL if no equalities are present)
        long[]? Ajc,    // Sparse A matrix column index array (column compressed storage) (can be all NULL if no equalities are present)
        long[]? Air,    // Sparse A matrix row index array (column compressed storage) (can be all NULL if no equalities are present)
        double[]? c,    // Array of size n, cost function weights
        double[]? h,    // Array of size m, RHS vector of cone constraint
        double[]? b     // Array of size p, RHS vector of equalities (can be NULL if no equalities are present)
    );

    [LibraryImport("ecos")]
    internal static partial int ECOS_solve(PWork* model);

    [LibraryImport("ecos")]
    internal static partial void ECOS_cleanup(PWork* model, long keepvars);

    // version

    [LibraryImport("ecos", StringMarshalling = StringMarshalling.Utf8)]
    [return: MarshalUsing(typeof(LibraryOwnedUtf8StringMarshaller))]
    internal static partial string? ECOS_ver();

    // expert

    [LibraryImport("ecos")]
    internal static partial void ecos_updateDataEntry_h(PWork* model, long idx, double value);

    [LibraryImport("ecos")]
    internal static partial void ecos_updateDataEntry_c(PWork* model, long idx, double value);

    [LibraryImport("ecos")]
    internal static partial void ECOS_updateData(PWork* model, double[]? Gpr, double[]? Apr, double[]? c, double[]? h, double[]? b);
}
