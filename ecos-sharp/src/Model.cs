
//  
//  Model.cs
//  ecos-sharp
//
//  Copyright 2024 Markus Uhr <uhrmar@gmail.com>.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in
//        the documentation and/or other materials provided with the
//        distribution.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

using System;

namespace Ecos;

public enum ExitCode
{
    OPTIMAL  = 0,  // Problem solved to optimality
    PINF     = 1,  // Found certificate of primal infeasibility
    DINF     = 2,  // Found certificate of dual infeasibility
    INACC_OFFSET = 10,  // Offset exitflag at inaccurate results
    MAXIT   = -1,  // Maximum number of iterations reached
    NUMERICS= -2,  // Search direction unreliable
    OUTCONE = -3,  // s or z got outside the cone, numerics?
    SIGINT  = -4,  // solver interrupted by a signal/ctrl-c
    FATAL   = -7,  // Unknown problem in solver
}

public unsafe class Model : IDisposable
{
    private readonly PWork* model;
    
    internal Model(PWork* objref)
    {
        this.model = objref;
    }

    public Model(long n, long m, long p, long l, long ncones, long[]? q, long nex, double[]? Gpr, long[]? Gjc, long[]? Gir, double[]? Apr, long[]? Ajc, long[]? Air, double[]? c, double[]? h, double[]? b)
        : this(NativeMethods.ECOS_setup(n, m, p, l, ncones, q, nex, Gpr, Gjc, Gir, Apr, Ajc, Air, c, h, b)) { }

    // public Model(int n, int m, int p, int l, int ncones, int[]? q, int nex, double[]? Gpr, int[]? Gjc, int[]? Gir, double[]? Apr, int[]? Ajc, int[]? Air, double[]? c, double[]? h, double[]? b)
    //     : this(NativeMethods.ECOS_setup(n, m, p, l, ncones, q, nex, Gpr, Gjc, Gir, Apr, Ajc, Air, c, h, b)) { }
    
    ~Model()
    {
        Dispose(false);
    }

    // internal IntPtr NativeModel
    // {
    //     get
    //     {
    //         if (this.disposed) {
    //             throw new ObjectDisposedException(null);
    //         }
    //         return (IntPtr)this.model;
    //     }
    // }
    
    // IDisposable API
    
    private bool disposed = false;
    
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    
    protected virtual void Dispose(bool disposing)
    {
        if (!this.disposed) {
            NativeMethods.ECOS_cleanup(this.model, 0);
            this.disposed = true;
        }
    }

    // settings

    public int Verbose
    {
        get => (int)this.model->settings->verbose;
        set => this.model->settings->verbose = value;
    }

    // stats

    public double PCost => this.model->info->pcost;
    public double DCost => this.model->info->dcost;


    // properties

    public long N => this.model->n;
    public long M => this.model->m;
	public long P => this.model->p;
    public long L => this.model->C->lpc->p;
    public long D => this.model->D;

    public long NSoc => this.model->C->nsoc;

    // variables
    public ReadOnlySpan<double> X => new ReadOnlySpan<double>(this.model->x, (int)this.model->n);
    public ReadOnlySpan<double> Y => new ReadOnlySpan<double>(this.model->y, (int)this.model->p);
    public ReadOnlySpan<double> Z => new ReadOnlySpan<double>(this.model->z, (int)this.model->m);


    // ECOS API

    public ExitCode Solve()
    {
        if (this.disposed) {
            throw new ObjectDisposedException(null);
        }
        return (ExitCode)NativeMethods.ECOS_solve(this.model);
    }

    // expert methods

    public void UpdateDataEntryH(int idx, double value)
    {
        if (this.disposed) {
            throw new ObjectDisposedException(null);
        }
        NativeMethods.ecos_updateDataEntry_h(this.model, idx, value);
    }

    public void UpdateDataEntryC(int idx, double value)
    {
        if (this.disposed) {
            throw new ObjectDisposedException(null);
        }
        NativeMethods.ecos_updateDataEntry_c(this.model, idx, value);
    }

    public void UpdateData(double[]? Gpr, double[]? Apr, double[]? c, double[]? h, double[]? b)
    {
        if (this.disposed) {
            throw new ObjectDisposedException(null);
        }
        NativeMethods.ECOS_updateData(this.model, Gpr, Apr, c, h, b);
    }

    // ECOS version

    public static string? Version()
    {
        return NativeMethods.ECOS_ver();
    }
}
