using System;
using System.IO;
using NUnit.Framework;

namespace Ecos.Tests;

[TestFixture]
public class TestGithubIssue98
{
    [Test]
    public void TestProblem()
    {
        long[] q = { 5 };

        long[] Gp = { 0, 4, 8, 12, 16, 17 };
        long[] Gi = { 0, 1, 2, 7, 0, 1, 2, 8, 3, 4, 5, 9, 3, 4, 5, 10, 6 };
        double[] Gx = {
            0.416757847405471,  2.136196095668454, 1.793435585194863, -1.0,
            0.056266827226329, -1.640270808404989, 0.841747365656204, -1.0,
            0.416757847405471,  2.136196095668454, 1.793435585194863, -1.0,
            0.056266827226329, -1.640270808404989, 0.841747365656204, -1.0, -1.0
        };

        double[] c = { 0.0, 0.0, 0.0, 0.0, 1.0 };
        double[] h = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

        using var mod = new Model(
            5, 11, 0,
            6, 1, q, 0,
            Gx, Gp, Gi,
            null, null, null,
            c, h, null
        );
        mod.Verbose = 0;

        /* solve */
        var exitcode = mod.Solve();

        Assert.AreEqual(ExitCode.OPTIMAL, exitcode, "ECOS failed to produce outputflag OPTIMAL");
    }
}
